package jaas.demo;

import com.kerb4j.server.marshall.Kerb4JException;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.Socket;
import java.net.ServerSocket;
import java.security.PrivilegedExceptionAction;
import java.util.concurrent.ExecutionException;
import java.util.logging.Level;
import java.util.logging.Logger;
import nl.mansoft.asn1types.GssAuth;
import nl.mansoft.asn1types.NegState;
import nl.mansoft.asn1types.NegStateEnum;
import nl.mansoft.asn1types.SpnegoRespToken;
import org.ietf.jgss.GSSContext;
import org.ietf.jgss.GSSCredential;
import org.ietf.jgss.GSSException;
import org.ietf.jgss.GSSManager;
import org.ietf.jgss.GSSName;
import org.ietf.jgss.Oid;

/**
 * A sample server application that uses JGSS to do mutual authentication
 * with a client using Kerberos as the underlying mechanism. It then
 * exchanges data securely with the client.
 *
 * Every message exchanged with the client includes a 4-byte application-
 * level header that contains the big-endian integer value for the number
 * of bytes that will follow as part of the JGSS token.
 *
 * The protocol is:
 *    1.  Context establishment loop:
 *         a. client sends init sec context token to server
 *         b. server sends accept sec context token to client
 *         ....
 *    2. client sends a wrap token to the server.
 *    3. server sends a wrap token back to the client.
 *
 * Start GssSpNegoServer first before starting GssClient.
 *
 * Usage:  java <options> GssSpNegoServer
 *
 * Example: java -Djava.security.auth.login.config=jaas-krb5.conf \
 *               GssSpNegoServer
 *
 * Add -Djava.security.krb5.conf=krb5.conf to specify application-specific
 * Kerberos configuration (different from operating system's Kerberos
 * configuration).
 */

public class GssSpNegoAuthServer  {
    private static final int PORT = 4567;
    private static final boolean VERBOSE = true;
    private static final int LOOP_LIMIT = 1000;
    private static int loopCount = 0;
    private static final String KEYTAB_EXT = ".keytab";
    private static final int KEYTAB_EXT_LENGTH = KEYTAB_EXT.length();
    private static final String RPC_QUEUE_NAME = "PIXIE.DEMO.ARPA2.ORG";

    public static void main(String[] args) throws Exception {

        //System.setProperty("java.security.auth.debug", "gssloginconfig");
        //System.setProperty("sun.security.krb5.debug", "true");
        //System.setProperty("sun.security.jgss.debug", "true");
        //System.setProperty("java.security.auth.login.config", "jaas.conf");
        //System.setProperty("java.security.krb5.conf", "arpa2.conf");

        //PrivilegedExceptionAction action = new GssServerAction(PORT);
        PrivilegedExceptionAction action = new GssServerAction(RPC_QUEUE_NAME);
        File directory = new File(".");
        File[] keytabs = directory.listFiles((File dir, String name) -> name.endsWith(KEYTAB_EXT));
        String[] names = new String[keytabs.length];
        for (int i = 0; i < keytabs.length; i++) {
            String keytab = keytabs[i].getName();
            names[i] = keytab.substring(0, keytab.length() - KEYTAB_EXT_LENGTH);
        }
        Jaas.loginAndAction(
            "HTTP",
            names
            , action
        );
    }

    static class GssServerAction implements PrivilegedExceptionAction {
        private int localPort;
        private ChallengeResponse rabbitAmqp;

        GssServerAction(int port) {
            this.localPort = port;
        }

        GssServerAction(String realm) {
            rabbitAmqp = new RabbitAmqp(realm);
        }

        private void writeResponse(DataOutputStream outStream, SpnegoRespToken spnegoRespToken, GSSName gssName) throws Kerb4JException, GSSException, IOException {
            GssAuth gssAuth = new GssAuth(spnegoRespToken, gssName);
            byte[] gssAuthToken = gssAuth.encode();
            if (VERBOSE) {
                System.out.println("Will send token of size " +
                    gssAuthToken.length + " from acceptSecContext.");
            }

            outStream.writeInt(gssAuthToken.length);
            outStream.write(gssAuthToken);
            outStream.flush();
        }

        private void writeAMQPResponse(SpnegoRespToken spnegoRespToken, GSSName gssName) throws Exception {
            GssAuth gssAuth = new GssAuth(spnegoRespToken, gssName);
            byte[] gssAuthToken = gssAuth.encode();
            if (VERBOSE) {
                System.out.println("Will send token of size " +
                    gssAuthToken.length + " from acceptSecContext.");
            }
            rabbitAmqp.publishResponse(gssAuthToken);
        }

        public Object runSocket() throws Exception {
            ServerSocket ss = new ServerSocket(localPort);

            // Get own Kerberos credentials for accepting connection
            GSSManager manager = GSSManager.getInstance();
            Oid spnegoOid = new Oid("1.3.6.1.5.5.2");
            GSSCredential serverCreds = manager.createCredential(null,
                                             GSSCredential.DEFAULT_LIFETIME,
                                             spnegoOid,
                                             GSSCredential.ACCEPT_ONLY);
            while (loopCount++ < LOOP_LIMIT) {

                System.out.println("Waiting for incoming connection...");

                try (Socket socket = ss.accept()) {
                    DataInputStream inStream =
                            new DataInputStream(socket.getInputStream());

                    DataOutputStream outStream =
                            new DataOutputStream(socket.getOutputStream());

                    System.out.println("Got connection from client " +
                            socket.getInetAddress());

                    /*
                    * Create a GSSContext to receive the incoming request
                    * from the client. Use null for the server credentials
                    * passed in. This tells the underlying mechanism
                    * to use whatever credentials it has available that
                    * can be used to accept this connection.
                    */

                    GSSContext context = manager.createContext(
                            (GSSCredential)serverCreds);

                    // Do the context establishment loop

                    while (!context.isEstablished()) {

                        if (VERBOSE) {
                            System.out.println("Reading ...");
                        }
                        byte[] token = new byte[inStream.readInt()];

                        if (VERBOSE) {
                            System.out.println("Will read input token of size " +
                                    token.length + " for processing by acceptSecContext");
                        }
                        inStream.readFully(token);

                        if (token.length == 0) {
                            if (VERBOSE) {
                                System.out.println("skipping zero length token");
                            }
                            continue;
                        }
                        if (VERBOSE) {
                            System.out.println("Token = " + getHexBytes(token));
                            System.out.println("acceptSecContext..");
                        }
                        try {
                            token = context.acceptSecContext(token, 0, token.length);
                        } catch (GSSException gssException) {
                            System.out.println("Exception in acceptSecContext: " + gssException.getMessage());
                            SpnegoRespToken spnegoRespToken = new SpnegoRespToken();
                            spnegoRespToken.setNegState(new NegState(NegStateEnum.REJECT));
                            writeResponse(outStream, spnegoRespToken, null);
                            break;
                        }

                        // Send a token to the peer if one was generated by
                        // acceptSecContext
                        if (token != null) {
                            SpnegoRespToken spnegoRespToken = new SpnegoRespToken(token);
                            GSSName gssName = context.isEstablished() ? context.getSrcName() : null;
                            writeResponse(outStream, spnegoRespToken, gssName);
                        }
                    }
                    if (context.isEstablished()) {
                        System.out.println("Context Established!");
                        System.out.println("Client principal is " + context.getSrcName());
                        System.out.println("Server principal is " + context.getTargName());

                        /*
                        * If mutual authentication did not take place, then
                        * only the client was authenticated to the
                        * server. Otherwise, both client and server were
                        * authenticated to each other.
                        */
                        if (context.getMutualAuthState()) {
                            System.out.println("Mutual authentication took place!");
                        }
                    } else {
                        System.out.println("Context NOT Established!");
                    }
                    System.out.println("Closing connection with client " +
                            socket.getInetAddress());
                    context.dispose();
                }
            }
            return null;
        }

        public Object runAMQP() throws Exception {
            // Get own Kerberos credentials for accepting connection
            GSSManager manager = GSSManager.getInstance();
            Oid spnegoOid = new Oid("1.3.6.1.5.5.2");
            GSSCredential serverCreds = manager.createCredential(null,
                                             GSSCredential.DEFAULT_LIFETIME,
                                             spnegoOid,
                                             GSSCredential.ACCEPT_ONLY);
            for (;;) {
                GSSContext context = manager.createContext(
                    (GSSCredential)serverCreds);
                // Do the context establishment loop
                while (!context.isEstablished()) {
                    try {
                        System.out.println("Waiting for incoming connection...");
                        //channel.basicAck(delivery.getEnvelope().getDeliveryTag(), false);
                        //channel.basicCancel(ctag);

                        byte[] token = rabbitAmqp.getChallenge();

                        if (VERBOSE) {
                            System.out.println("Will read input token of size " +
                                    token.length + " for processing by acceptSecContext");
                        }

                        if (token.length == 0) {
                            if (VERBOSE) {
                                System.out.println("skipping zero length token");
                            }
                        }
                        if (VERBOSE) {
                            System.out.println("Token = " + getHexBytes(token));
                            System.out.println("acceptSecContext..");
                        }
                        try {
                            token = context.acceptSecContext(token, 0, token.length);
                        } catch (GSSException gssException) {
                            System.out.println("Exception in acceptSecContext: " + gssException.getMessage());
                            SpnegoRespToken spnegoRespToken = new SpnegoRespToken();
                            spnegoRespToken.setNegState(new NegState(NegStateEnum.REJECT));
                            try {
                                writeAMQPResponse(spnegoRespToken, null);
                            } catch (Kerb4JException ex) {
                                Logger.getLogger(GssSpNegoAuthServer.class.getName()).log(Level.SEVERE, null, ex);
                            } catch (GSSException ex) {
                                Logger.getLogger(GssSpNegoAuthServer.class.getName()).log(Level.SEVERE, null, ex);
                            } catch (IOException ex) {
                                Logger.getLogger(GssSpNegoAuthServer.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }

                        // Send a token to the peer if one was generated by
                        // acceptSecContext
                        if (token != null) {
                            try {
                                SpnegoRespToken spnegoRespToken = new SpnegoRespToken(token);
                                GSSName gssName = context.isEstablished() ? context.getSrcName() : null;
                                writeAMQPResponse(spnegoRespToken, gssName);
                            } catch (Kerb4JException ex) {
                                Logger.getLogger(GssSpNegoAuthServer.class.getName()).log(Level.SEVERE, null, ex);
                            } catch (GSSException ex) {
                                Logger.getLogger(GssSpNegoAuthServer.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }
                    } catch (RuntimeException e) {
                        System.out.println(" [.] " + e);
                    } finally {
                    }
                }
                if (context.isEstablished()) {
                    System.out.println("Context Established!");
                    System.out.println("Client principal is " + context.getSrcName());
                    System.out.println("Server principal is " + context.getTargName());

                    /*
                    * If mutual authentication did not take place, then
                    * only the client was authenticated to the
                    * server. Otherwise, both client and server were
                    * authenticated to each other.
                    */
                    if (context.getMutualAuthState()) {
                        System.out.println("Mutual authentication took place!");
                    }
                } else {
                    System.out.println("Context NOT Established!");
                }
                context.dispose();
            }
        }

        @Override
        public Object run() throws Exception {
            return runAMQP();
        }
    }

    private static String getHexBytes(byte[] bytes, int pos, int len) {

        StringBuilder sb = new StringBuilder();
        for (int i = pos; i < (pos+len); i++) {

            int b1 = (bytes[i]>>4) & 0x0f;
            int b2 = bytes[i] & 0x0f;

            sb.append(Integer.toHexString(b1));
            sb.append(Integer.toHexString(b2));
            sb.append(' ');
        }
        return sb.toString();
    }

    private static String getHexBytes(byte[] bytes) {
        return getHexBytes(bytes, 0, bytes.length);
    }
}