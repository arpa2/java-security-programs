/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package jaas.demo;

import javax.security.auth.Subject;
import javax.security.auth.callback.CallbackHandler;
import com.sun.security.auth.callback.TextCallbackHandler;
import java.security.Principal;
import java.security.PrivilegedActionException;
import java.security.PrivilegedExceptionAction;
import java.util.HashSet;
import java.util.Set;
import javax.security.auth.login.Configuration;
import javax.security.auth.login.LoginContext;
import javax.security.auth.login.LoginException;

public class Jaas {
    private static String name;
    private static final boolean verbose = false;
    private static Set<LoginContext> contexts = new HashSet<>();

    public static void main(String[] args) throws Exception {
        if (args.length > 0) {
            name = args[0];
        } else {
            name = "client";
        }

        // Create action to perform
        PrivilegedExceptionAction action = new MyAction();

        loginAndAction("host", name, action);
    }

    static void login(String name, Subject subject, CallbackHandler callbackHandler, Configuration config) throws LoginException {
        // Create a LoginContext with a callback handler
        LoginContext context = new LoginContext(name, subject, callbackHandler, config);

        // Perform authentication
        context.login();
        contexts.add(context);

    }

    public static Object loginAndAction(String service, String[] names, PrivilegedExceptionAction action)
        throws LoginException, PrivilegedActionException {

        // Create a callback handler
        CallbackHandler callbackHandler = new TextCallbackHandler();
        DynamicConfiguration dynamicConfiguration = new DynamicConfiguration(service);
        Subject subject = new Subject();
        try {
            for (String name: names) {
                login(name, subject, callbackHandler, dynamicConfiguration);
            }
            //login("server.local", subject, callbackHandler, dynamicConfiguration);
        } catch (LoginException e) {
            System.err.println("Login failed");
            e.printStackTrace();
            System.exit(-1);
        }

        if (verbose) {
            System.out.println(subject.toString());
        } else {
            String principals = "Authenticated principals: ";
            for (Principal principal : subject.getPrincipals()) {
                principals += principal.getName() + " ";
            }
            System.out.println(principals);
        }

        Object obj = Subject.doAs(subject, action);

        for (LoginContext context: contexts) {
            context.logout();
        }
        contexts.clear();
        return obj;
    }

    public static Object loginAndAction(String service, String name, PrivilegedExceptionAction action)
        throws LoginException, PrivilegedActionException {
        return loginAndAction(service, new String[] { name }, action);
    }

    // Action to perform
    static class MyAction implements PrivilegedExceptionAction {
        MyAction() {
        }

        @Override
        public Object run() throws Exception {
            // Replace the following with an action to be performed
            // by authenticated user
            System.out.println("Performing secure action ...");
            return null;
        }
    }
}