/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package jaas.demo;

/**
 *
 * @author hfman
 */
public interface ChallengeResponse {
    public byte[] getChallenge() throws Exception;
    public void publishResponse(byte[] challengeResponse) throws Exception;
}
