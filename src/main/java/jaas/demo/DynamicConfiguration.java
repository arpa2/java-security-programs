package jaas.demo;

import java.util.HashMap;
import java.util.Map;
import javax.security.auth.login.AppConfigurationEntry;
import javax.security.auth.login.Configuration;

/**
 *
 * @author hfman
 */
public class DynamicConfiguration extends Configuration {

    private final String service;

    public DynamicConfiguration(String service) {
        this.service = service;
    }

    @Override
    public AppConfigurationEntry[] getAppConfigurationEntry(String name) {
        Map<String, Object> options = new HashMap<>();
        if (service == null) {
            options.put("useTicketCache", "false");
            options.put("principal", name);
        } else {
            options.put("useKeyTab", "true");
            options.put("storeKey", "true");
            options.put("keyTab", name + ".keytab");
            options.put("principal", name.replaceFirst("\\.", "/"));
        }
        AppConfigurationEntry appConfigurationEntry = new AppConfigurationEntry(
            "com.sun.security.auth.module.Krb5LoginModule",
            AppConfigurationEntry.LoginModuleControlFlag.REQUIRED,
            options
        );

        return new AppConfigurationEntry[] {
            appConfigurationEntry
        };
    }
}
