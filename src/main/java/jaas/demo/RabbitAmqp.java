/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package jaas.demo;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;
import com.rabbitmq.client.Delivery;
import java.io.IOException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author hfman
 */
public class RabbitAmqp implements ChallengeResponse, DeliverCallback {
    private Connection connection;
    private Channel channel;
    private CompletableFuture<Delivery> response;
    AMQP.BasicProperties deliveryProps;

    public RabbitAmqp(String realm) {
        try {
            ConnectionFactory factory = new ConnectionFactory();
            factory.setHost("manson-virtualbox");

            connection = factory.newConnection();
            channel = connection.createChannel();
            channel.queueDeclare(realm, false, true, false, null);

            channel.basicQos(1);
            channel.basicConsume(realm, true, this, (String consumerTag) -> {
                System.err.println(consumerTag + " canceled");
            });
        } catch (IOException ex) {
            Logger.getLogger(GssSpNegoAuthServer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (TimeoutException ex) {
            Logger.getLogger(GssSpNegoAuthServer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public byte[] getChallenge() throws InterruptedException, ExecutionException {
        response = new CompletableFuture<>();
        Delivery delivery = response.get();
        deliveryProps = delivery.getProperties();
        return delivery.getBody();
    }

    @Override
    public void publishResponse(byte[] challengeResponse) throws IOException, InterruptedException, ExecutionException {
        AMQP.BasicProperties replyProps = new AMQP.BasicProperties
            .Builder()
            .correlationId(deliveryProps.getCorrelationId())
            .build();

        channel.basicPublish("", deliveryProps.getReplyTo(), true, replyProps, challengeResponse);
    }

    @Override
    public void handle(String consumerTag, Delivery delivery) throws IOException {
        if (response != null) {
            response.complete(delivery);
        }
    }
}