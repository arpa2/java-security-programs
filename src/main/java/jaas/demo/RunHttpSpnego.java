/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package jaas.demo;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.Authenticator;
import java.net.PasswordAuthentication;
import java.net.URL;

public class RunHttpSpnego {

    static final String kuser = "mary"; // your account name
    static final String kpass = "sekreet456"; // your password for the account

    static class MyAuthenticator extends Authenticator {

        @Override
        public PasswordAuthentication getPasswordAuthentication() {
// I haven't checked getRequestingScheme() here, since for NTLM
// and Negotiate, the usrname and password are all the same.
            System.err.println("Feeding username and password for "
                    + getRequestingScheme());
            return (new PasswordAuthentication(kuser, kpass.toCharArray()));
        }
    }

    public static void main(String[] args) throws Exception {
        //System.setProperty("java.security.krb5.conf", "arpa2.conf");
        //System.setProperty("javax.security.auth.useSubjectCredsOnly", "false");
        //System.setProperty("sun.security.krb5.debug", "true");
        System.setProperty("sun.security.jgss.native", "false");
        Authenticator.setDefault(new MyAuthenticator());
        URL url = new URL("http://desktop-g22703r:8080/HttpSpNegoForward/Authenticate");
        //URL url = new URL("https://mansoft.nl/diagss/index.html");
        InputStream ins = url.openConnection().getInputStream();
        BufferedReader reader = new BufferedReader(new InputStreamReader(ins));
        String str;
        while ((str = reader.readLine()) != null) {
            System.out.println(str);
        }
    }
}
