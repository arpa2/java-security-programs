/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package jaas.demo;

import com.kerb4j.server.marshall.Kerb4JException;
import com.kerb4j.server.marshall.spnego.SpnegoInitToken;
import com.kerb4j.server.marshall.spnego.SpnegoKerberosMechToken;
import java.io.IOException;
import java.util.Base64;
import java.util.logging.Level;
import java.util.logging.Logger;
import nl.mansoft.asn1types.GssAuth;
import nl.mansoft.asn1types.GssName;
import nl.mansoft.asn1types.SpnegoRespToken;
import org.apache.kerby.asn1.util.HexUtil;
import org.apache.kerby.kerberos.kerb.type.ap.ApReq;
import org.apache.kerby.kerberos.kerb.type.base.PrincipalName;
import org.apache.kerby.kerberos.kerb.type.ticket.Ticket;
import org.ietf.jgss.GSSException;
import org.ietf.jgss.GSSManager;
import org.ietf.jgss.GSSName;
import org.ietf.jgss.Oid;

/**
 *
 * @author hfman
 */
public class DecodeApReq {
    static String apdata = "6e82026d30820269a003020105a10302010ea20703050000000000a38201796182017530820171a003020105a1161b1450495849452e44454d4f2e41525041322e4f5247a21f301da003020103a11630141b04485454501b0c636c69656e742e6c6f63616ca382012f3082012ba003020112a103020102a282011d0482011957734c9887bc6faabb8e17c5dff6bc8a3955127e167f0105c2ed88c5148a8c1b79ab5d2d8fe5bc832ef36ed51ff2c6c9ded3aee71a4e6471f5bb53f52de85e5d22d0c6781832c6293536c0aca3b1970b9faa109c1e3491f31b88308c53e48f86ee90221b49e90cab58e805138ed701f537ec80b7c37d76717f62537487c9415418ba69ee5452597b3fdc808efa07403594382c240a31d892d94c49ec6235116d2c1cf3e04e2844139cb1eac5d071bd1f1999d8ec981ff2e378453e148e56f834229f2e8d24eaf99951b1654fc352e2072bbb3e6d067f0cf86e42bd76551e730136995b222fd85ca637f6c299ad13578f5f8c91e1e722c5a44a871f65888053daf9fa16f85ce537d86b26c14deb5db313f21c7e5e7a56f77f3fa481d63081d3a003020112a281cb0481c868bfc41f09b87f8aa69aa29e1a8f36d01ce98f0c81392476bb2476df1547d29183114bcf07437a3da42b694b2f19291d189007a3ea0ca2ba32923d595f74bab5380cd2d076d200ede6e6739d4fbbee2fef4950e174135ec189eec512f9840f1eb710ae69d0bc5bb72f9e8480375701c6cc3243809e9bb121d9707ab7f5152e739d0ab9aa76515eea70957f02a0760a5301fd5359a165b4e5c787f792c2d6b87f3f44793f30df44e74be7e8d30d998ce8182e8c28acf67d4b36110a937c387e9fb6610744963e9919";
    static String negosp = "YIIClgYGKwYBBQUCoIICijCCAoagDTALBgkqhkiG9xIBAgKiggJzBIICb2CCAmsGCSqGSIb3EgECAgEAboICWjCCAlagAwIBBaEDAgEOogcDBQAgAAAAo4IBZmGCAWIwggFeoAMCAQWhFhsUUElYSUUuREVNTy5BUlBBMi5PUkeiHzAdoAMCAQOhFjAUGwRIVFRQGwxjbGllbnQubG9jYWyjggEcMIIBGKADAgESoQMCAQKiggEKBIIBBpXBzOM3/DgOzEQK91BoaEVGNDEmFBTENnyw2iAbJerGf7AILPoukYFAK5hpnp+A8ytUfEbAsbAuVu4g/Oi+t+YqosKM9206Uk7JORrmmFrU0nvN2qUdNLKr8wi3M/yJtbEeunQ8YHt4R1eqp2bOZDKomjsh0YjIwOTLv1Zquj/N1oOWoya8JzoOhfjIqBrBi2XUhzTiHvU4SlB55oqKGNLNe58GZOSJTfknR48oVTMRiIxVIQd5F3cdC7/FB//a9WrKwa6p6VZBBv4Ca7sN5wh1rHDgpfgN7myBqf5kJrLWiNueiKuN8aYkdVolea42KHVGZ1+SCdWfdBmJJT3hw4ktCqv71pukgdYwgdOgAwIBEqKBywSByFo4ErhAQlU622yTarv2yUj8je33rhpHLHynXPluCF5OtEDfHJYVjDl+dnLscceS4DKJ3ViPUhM5phrInQ5h0PjOmsEAFnYWYvF6edSqbfLp/O3m81AS2EOMDwEdFuBuPHgiPTEBQtNrGg7KUyuUV0XV40RzwgIfEoa6MUSeh83mJaDrkMoDXVNI7Su9fDqQKZv/2/ZjDB3HP8HzUoULiP2/mbN35DnRfJDiknjMNPc6EN2TKwhzME0tPnLU+Wj/qaLb3gnXOvY6";
    static String negTokenRespBase64 = "oRQwEqADCgEAoQsGCSqGSIb3EgECAg==";

    public static void decodeApReq(ApReq apReq) throws IOException {
        Ticket ticket = apReq.getTicket();
        String realm = ticket.getRealm();
        System.out.println("ticket Realm: " + realm);
        PrincipalName principalName = ticket.getSname();
        System.out.println("sname principal name: " + principalName.getName());
    }

    public static void decodeApReq() throws IOException {
        byte[] ap = HexUtil.hex2bytes(apdata);
        ApReq apReq = new ApReq();
        apReq.decode(ap);
        decodeApReq(apReq);
    }

    public static void decodeSpNego()  throws IOException, GSSException, Kerb4JException {
        byte[] negotiation = Base64.getDecoder().decode(negosp);
        SpnegoInitToken spnegoInitToken = new SpnegoInitToken(negotiation);
        byte[] mechToken = spnegoInitToken.getMechToken();
        SpnegoKerberosMechToken spnegoKerberosMechToken = new SpnegoKerberosMechToken(mechToken);
        ApReq apReq = spnegoKerberosMechToken.getApRequest();
        decodeApReq(apReq);
    }

    public static void decodeNegTokenResp() throws IOException, Kerb4JException {
        byte[] negTokenResp = Base64.getDecoder().decode(negTokenRespBase64);
        SpnegoRespToken spnegoRespToken = new SpnegoRespToken(negTokenResp);
        System.err.println(spnegoRespToken.getNegState());
        System.err.println(spnegoRespToken.getSupportedMech());
    }

    public static byte[] encodeGssAuth() throws IOException, Kerb4JException, GSSException {
        GSSManager manager = GSSManager.getInstance();
        GSSName gssName = manager.createName("henri@manson", GSSName.NT_USER_NAME);
        byte[] negTokenResp = Base64.getDecoder().decode(negTokenRespBase64);
        SpnegoRespToken spnegoRespToken = new SpnegoRespToken(negTokenResp);
        GssAuth gssAuth = new GssAuth(spnegoRespToken, gssName);
        byte data[] = gssAuth.encode();
        System.err.println(HexUtil.bytesToHex(data));
        return data;
    }

    public static void decodeGssAuth(byte[] data) throws IOException, Kerb4JException, GSSException {
        GssAuth gssAuth = new GssAuth(data);
        SpnegoRespToken spnegoRespToken = gssAuth.getSpnegoRespToken();
        GssName gssName = gssAuth.getGssName();
        GSSManager manager = GSSManager.getInstance();
        GSSName name = manager.createName(gssName.getName(), new Oid(gssName.getNameType()));
        System.out.println(name.toString());
        System.out.println(name.getStringNameType().toString());
    }

    public static void main(String[] args)  {
        try {
            //decodeApReq();
            //decodeSpNego();
            //decodeNegTokenResp();
            byte data[] = encodeGssAuth();
            decodeGssAuth(data);
        } catch (Exception ex) {
            Logger.getLogger(DecodeApReq.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
